package com.example.rappelsjava8;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

@SpringBootTest
public class ReactorTest {

    @Test
    public void setVerifier() {


        StepVerifier
                .create(Flux.just(1,2,3))
                .expectNext(1)
                .expectNext(2)
                .expectNext(3)
                .expectComplete()
                .verify();
    }

    @Test
    public void stepVerifier2() {
        StepVerifier
                .create(Flux.range(0, 10000))
                .expectNextCount(10000)
                .verifyComplete();
    }

    @Test
    public void stepVerifier3() {
        StepVerifier
                .withVirtualTime(() -> Flux
                        .range(0, 12)
                        .delayElements(Duration.ofHours(1)))
                .thenAwait(Duration.ofHours(10))
                .expectNextCount(10)
                .thenAwait(Duration.ofHours(2))
                .expectNextCount(4)
                .verifyComplete();
    }
}
