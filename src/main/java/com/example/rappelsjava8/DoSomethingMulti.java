package com.example.rappelsjava8;

@FunctionalInterface
public interface DoSomethingMulti {
    int doAThing(int a, int b);
}
