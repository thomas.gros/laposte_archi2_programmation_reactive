package com.example.rappelsjava8;

@FunctionalInterface
public interface DoSomething {
    String doAThing();
}
