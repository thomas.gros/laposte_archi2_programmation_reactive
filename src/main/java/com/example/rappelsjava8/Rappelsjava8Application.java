package com.example.rappelsjava8;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

@SpringBootApplication
public class Rappelsjava8Application {

//	@Bean
//	public CommandLineRunner test() {
//
////		return new CommandLineRunner() {
////
////			@Override
////			public void run(String... args) throws Exception {
////				System.out.println("hello");
////			}
////		};
//
//
//		return args -> {
//			System.out.println("hello");
//
//			DoSomething d1 = () -> {
//				System.out.println("dans d1");
//				return "fin...";
//			};
//
//			d1.doAThing();
//
//			DoSomething d2 = () -> "ceci est d2";
////			DoSomething d2 = () -> {
////				return "ceci est d2";
////			};
//
//			DoSomethingMulti dm = (a,b) -> a + b;
//
//			System.out.println(dm.doAThing(2,4));
//
//
//
//
//		};
//	}
//
//	@Bean
//	public CommandLineRunner commonFI() {
//		return args -> {
//
//			Predicate<String> isNotEmpty = s -> s.length() > 0;
//			System.out.println("hello is not empty " + isNotEmpty.test("hello"));
//			System.out.println("not(hello is not empty) " + isNotEmpty.negate().test("hello"));
//
//
//			Function<String, Integer> length = s -> s.length();
//
//			System.out.println(length.apply("hello"));
//
//
//			Supplier<Integer> supplier = () -> 42;
//			Consumer<Integer> consumer = (i) -> System.out.println(i);
//
//		};
//	}
//
//
//	@Bean
//	public CommandLineRunner streamDemo() {
//		return args -> {
//			List<String> data = Arrays.asList("cat", "dog", "mouse", "chiwawa");
//
////			Stream<String> s = data.stream();
////			Stream<String> s2 = s.map(d -> d.toUpperCase()); // map n input => n output
////			Stream<String> s3 = s2.filter(d -> d.startsWith("C"));
////			s3.forEach(d -> System.out.println(d));
//
//
//			Stream<String> pipeline = data.stream()
//									 // data.parallelStream()
//					 					  .map(d -> d.toUpperCase())
//										  .filter(d -> d.startsWith("C"));
//
//			// pipeline.forEach(d -> System.out.println(d));
//			pipeline.forEach(System.out::println); // method reerence
//
//			data.stream()
//					.map(String::toUpperCase)
//					.filter(d -> d.startsWith("C"))
//					.forEach(System.out::println);
//
//
//		};
//	}


	public static void main(String[] args) {
		SpringApplication.run(Rappelsjava8Application.class, args);
	}

}