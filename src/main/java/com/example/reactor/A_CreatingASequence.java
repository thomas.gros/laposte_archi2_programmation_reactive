package com.example.reactor;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Optional;


//@Component
public class A_CreatingASequence implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        // FLUX
        System.out.println("### JUST");
        Flux<Integer> f1 = Flux.just(1,2,3,4,5); // Flux<Integer>

        f1.subscribe(
                e -> System.out.println(e), // chaque element
                e -> { e.printStackTrace(); }, // erreur
                () -> System.out.println("completed")); // succès



        //Flux.just("h", "e", "l", "l", "o"); // Flux<String>
        System.out.println("### arrays");
        Integer[] ints = { 1, 2, 3};
        Flux
                .fromArray(ints)
                .subscribe(System.out::println);

        System.out.println("### streams");
        Flux
                .fromStream(Arrays.asList(1,2,3).stream())
                .subscribe(System.out::println);


        // MONO
        System.out.println("### mono");
        Mono.just("hello").subscribe(System.out::println);
        Mono.justOrEmpty("hello").subscribe(
                e -> System.out.println(e), // chaque element
                e -> { e.printStackTrace(); }, // erreur
                () -> System.out.println("completed")); // succès

        Mono.justOrEmpty(null).subscribe(System.out::println);
        Mono.justOrEmpty(Optional.empty()).subscribe(
                e -> System.out.println(e), // chaque element
                e -> { e.printStackTrace(); }, // erreur
                () -> System.out.println("completed")); // succès


    }
}
