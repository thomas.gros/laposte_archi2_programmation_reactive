package com.example.reactor;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;

// @Component
public class D_PeekingIntoASequence implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
//        Flux
//                .just(1,2,3,4)
//                .doOnNext(e -> System.out.println("just -> " + e))
//                .onErrorContinue((e, o) -> System.out.println(e))
//                .map(e -> e * 2)
//                .subscribe(
//                        System.out::println,
//                        e -> e.printStackTrace()
//                );

//        Flux
//                .just(1,2,3,4)
//                .log()
//                .map(e -> e * 2)
//                .log()
//                .subscribe(
//                        System.out::println,
//                        e -> e.printStackTrace()
//                );

        // Hooks.onOperatorDebug();

        Flux
                .just(1,2,3,4)
                .map(e -> {
                    if (e > 3) { throw new RuntimeException(); }
                    return e;
                })
                .checkpoint()
          //      .log()
                .subscribe(
                        System.out::println,
                        e -> e.printStackTrace()
                );
    }
}
