package com.example.reactor;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.Duration;

// @Component
public class F_Multisubscriber implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        // Flux<Integer> source = Flux.just(1,2,3).delayElements(Duration.ofSeconds(1));
//        Flux<Integer> source = Flux.just(1,2,3)
//                                   .delayElements(Duration.ofSeconds(1))
//                                   .share();
//        Flux<Long> startFlux = mongoRepo.findAll()
//                //.interval(Duration.ofMillis(200));
//                .share(); // share <=>  publish().ConnectableFlux.refCount(), voir https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html#share--. Le flux est transformé en 'hot flux', cf plus bas avec les delais
//
//        Flux.from(startFlux).subscribe();
//        Flux.from(startFlux).subscribe();
//
//        startFlux.subscribe(l -> System.out.println("sub1 " + l));
//        Thread.sleep(5000);
//        startFlux.subscribe(l -> System.out.println("sub2 " + l));
//
//        System.in.read();
    }
}

