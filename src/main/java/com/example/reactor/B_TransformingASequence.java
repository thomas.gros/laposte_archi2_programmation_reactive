package com.example.reactor;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.ArrayList;

// @Component
public class B_TransformingASequence implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

//        Flux
//                .just(1,2,3)
//                .map(d -> d * 2)
//                .subscribe( e -> System.out.println(e), // chaque element
//                            e -> { e.printStackTrace(); }, // erreur
//                            () -> System.out.println("completed")); // succès

//        Flux
//                .just(new Person("John", "Doe"), new Person("Sally", "Smith"))
//                .map(p -> p.getFirstname() + " " + p.getLastname())
//                .subscribe( e -> System.out.println(e), // chaque element
//                        e -> { e.printStackTrace(); }, // erreur
//                        () -> System.out.println("completed")); // succès

//        Disposable d = Flux.just(1,2,3).subscribe();
////        d.dispose();

//
//        Flux
//                .range(0,10)
//                .startWith(10,20,30)
//                .startWith(100,200, 300)
//                .subscribe(System.out::println);

//        Flux
//                .range(0,10)
//                .concatWith(Flux.range(100, 10))
//                .subscribe(System.out::println);

//        Flux
//                .interval(Duration.ofMillis(500))
//                .startWith(1000L,2000L)
//                .subscribe(System.out::println);
//
//        System.in.read();

//        Flux
//                .range(0,10)
//                .timestamp()
//                .subscribe(System.out::println);
//
//        Flux
//                .range(0, 10)
//                .collectList()
//                .subscribe(System.out::println);

//        Flux
//                .range(0, 10)
//                .count()
//                .subscribe(System.out::println);

//        Flux
//                .range(0, 10)
//                .reduce(0, (acc, ele) -> acc + ele)
//                .subscribe(System.out::println);


//        Flux
//            .just(new Person("John", "Doe"), new Person("Sally", "Smith"))
//            .map(p -> p.getFirstname() + " " + p.getLastname())
//            .reduce("", (html, s) -> html + "<li>" + s + "</li>")
//                .concatWith(Flux.just("</ul>"))
//                .startWith("<ul>")
//            .subscribe(System.out::println);

//        Flux
//                .just(new Person("John", "Doe"), new Person("Sally", "Smith"))
//                .map(p -> p.getFirstname() + " " + p.getLastname())
//                .reduce("<ul>", (html, s) -> html + "<li>" + s + "</li>")
//                .map(html -> html + "</ul>")
//                .subscribe(System.out::println);

//        Flux
//            .range(0, 10)
//            .scan(0, (acc, ele) -> acc + ele)
//            .subscribe(System.out::println);

//        Flux
//            .just(new Person("John", "Doe"), new Person("Sally", "Smith"))
//            .map(p -> p.getFirstname() + " " + p.getLastname())
//            .scan("<ul>", (html, s) -> html + "<li>" + s + "</li>")
//            .map(html -> html + "</ul>")
//            .subscribe(System.out::println);

//            Flux
//                .range(0, 10)
//                .delayElements(Duration.ofMillis(300))
//                .mergeWith(Flux
//                        .range(100, 10)
//                        .delayElements(Duration.ofMillis(500)))
//                .subscribe(System.out::println);

//        Flux.mergeSequential(
//                Flux.range(0, 10).delayElements(Duration.ofMillis(300)),
//                Flux.range(100, 10).delayElements(Duration.ofMillis(500))
//        ).subscribe(System.out::println);


//                Flux
//                        .range(0, 10)
//                        .delayElements(Duration.ofMillis(500))
//                        .subscribe(System.out::println);

//        Flux.combineLatest(
//                Flux.just("a", "b", "c").delayElements(Duration.ofMillis(200)),
//                Flux.just("x", "y", "z").delayElements(Duration.ofMillis(500)),
//                (e1, e2) -> e1 + e2
//        ).subscribe(System.out::println);

//        Flux
//                .just("a", "b", "c")
//                .zipWith(Flux.just(1,2,3,4))
//                .subscribe(System.out::println);


//        Flux
//                .just(1,2,3)
//                .map(e -> Flux.range(0, e))
//                .subscribe(System.out::println);

//        Flux
//                .just(1,2,3)
//                .flatMap(e -> Flux.range(0, e))
//                 .subscribe(System.out::println);

        // scatter-gather
//        Flux
//                .just("http://www.example.com", "http://httpbin.org")
//                .flatMap(url -> WebClient
//                                    .create(url)
//                                    .get()
//                                    .uri("/")
//                                    .retrieve()
//                                    .bodyToFlux(String.class)
//                                    .map(html -> Tuples.of(url, html))
//                .subscribe(System.out::println);
//
//
//                System.in.read();

//        Flux
//                .range(0, 15)
//                .buffer(6)
//                .subscribe(System.out::println);

//        Flux
//                .just(1,2,3)
//                .repeat(4)
//                .subscribe(System.out::println);

//        Flux
//                .just(1,2,3)
//                .all(e -> e > 2) // voir any
//                .subscribe(System.out::println);

//        Flux
//                .range(0, 15)
//                .buffer(6)
//                .subscribe(System.out::println);

//        Flux
//            .range(0, 15)
//            .delayElements(Duration.ofMillis(200)) // Signals are delayed and continue on the parallel default Scheduler,
//            .window(Duration.ofMillis(500), Duration.ofMillis(300))
//            .flatMap(
//                    window -> window.reduce(new ArrayList<>(), (a,b) -> { a.add(b); return a;})
//            )
//            .subscribe(System.out::println);

//        Flux
//                .range(0, 15)
//                .filter(e -> e % 2 == 0)
//                .subscribe(System.out::println);

//        Flux
//                .just(1,2,3,1,2,3, 4, 5)
//                .distinct()
//                .subscribe(System.out::println);
//        Flux
//                .range(0, 15)
//                .take(10)
//                .subscribe(System.out::println);
//        Flux
//                .range(0, 15)
//                .takeUntil(e -> e > 8)
//                .subscribe(System.out::println);

//        Flux
//                .range(0, 15)
//                .takeLast(3)
//                .subscribe(System.out::println);

//        Flux
//                .range(0, 15)
//                .skip(5)
//                .subscribe(System.out::println);

//        Flux
//            .range(0, 15)
//            .delayElements(Duration.ofMillis(200))
//            .sample(Duration.ofMillis(300))
//            .subscribe(System.out::println);
//
//            System.in.read();

//            Flux.just("a", "b", "c", "d", "e")
//                    .index()
//                    .skip(2)
//                    .subscribe(System.out::println);



    }
}
