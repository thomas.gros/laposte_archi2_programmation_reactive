package com.example.reactor;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;

@Component
public class E_Threading implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
//        Flux
//                .just(1,2,3)
//                .log()
//                .subscribe(System.out::println);

//        Flux
//                .interval(Duration.ofMillis(100))
//	            .log()
//	            .subscribe(i -> System.out.println(Thread.currentThread().getName()));

//        Scheduler scheduler = Schedulers.newElastic("foo");
//        Flux<Integer> flux = Flux.just(1, 2, 3)
//			.log()
//		    .subscribeOn(scheduler);
//
//        flux.subscribe(i -> System.out.println(Thread.currentThread().getName()));
//
//        System.in.read();

//        Flux
//				.just(1,2,3,4,5)
//				.publishOn(Schedulers.single())
//				.map(d -> d * 5)
//                .log()
//				.publishOn(Schedulers.newElastic("foo"))
//                .log()
//                .subscribeOn(Schedulers.newElastic("bar"))
//				.subscribe(i -> System.out.println(Thread.currentThread().getName()));

        // mongoRepo.findAll().subscribeOn(...).subsccribe()
//			Flux
//				.just(1,2,3,4,5)
//				//.publishOn(Schedulers.single())
//				.map(d -> d * 5)
//				.publishOn(myUI::thread())
//                .subscribeOn(Schedulers.single())
//				.subscribe(System.out::println);
//        Flux
//			.range(0, 20)
//			.parallel()
//			.runOn(Schedulers.parallel())
//			.map(d -> d * 2)
//			.sequential()
//			.subscribe(System.out::println);

//        Integer i = Flux.range(0, 20).blockFirst(); // éviter block, par fois utile pour s'interfacer avec legacy code
    }
}
