package com.example.reactor;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuples;

import java.sql.SQLException;
import java.time.Duration;

//@Component
public class C_ErrorHandling implements CommandLineRunner {


    @Override
    public void run(String... args) throws Exception {
//        Flux
//                .just(1,2,3,4,5,6,3)
//                .map(e -> {
//                    if(e < 4) return e * 2;
//                    throw new RuntimeException("boom");
//                })
//                .subscribe(
//                        System.out::println,
//                        e -> e.printStackTrace());

//        Flux
//                .just(1,2,3,4,5,6,3)
//                .map(e -> {
//                    if(e < 4) return e * 2;
//                    throw new RuntimeException("boom");
//                })
//                .onErrorReturn(-1)
//                .subscribe(
//                        System.out::println,
//                        e -> e.printStackTrace());

        Flux
                .just(1,2,3,4,5,6,3)
                .map(e -> {
                    if(e < 4) return e * 2;
                    throw new RuntimeException("boom");
                })
                .onErrorResume(e -> Flux.just(10, 20, 30))
                .subscribe(
                        System.out::println,
                        e -> e.printStackTrace());

//        userMovieService
//                .getFavoritesForUser(userId)
//                .onErrorResume(userLocalCachedFavorites.getCachedFavorite())

//        Flux
////                .just(1,2,3,4)
////                .map(e -> {
////                    if(e < 4) return e * 2;
////                    throw new RuntimeException("boom");
////                })
////                .retry(2)
////                .subscribe(
////                        System.out::println,
////                        e -> e.printStackTrace()
////                );

//        Flux
//                .just(1,2,3,4)
//                .map(e -> {
//                    if(e < 4) return e * 2;
//
//                    // classes utilitaires Exceptions.propagate(e)
//                    try {
//                        throw new SQLException();
//                    } catch (Exception ex) {
//                        throw new RuntimeException(ex);
//                    }
//                })
//                .retry(2)
//                .subscribe(
//                        System.out::println,
//                        e -> e.printStackTrace()
//                );

//        Flux
//                .just(1,2,3,4)
//                .delayElements(Duration.ofSeconds(1))
//                .timeout(Duration.ofMillis(500))
//                .subscribe(
//                        System.out::println,
//                        e -> e.printStackTrace()
//                );

        Flux
                .empty()
                .switchIfEmpty(Flux.just(1,2,3))
                .subscribe(
                        System.out::println,
                        e -> e.printStackTrace()
                );

//		userMovieService
    //		.getFavoritesForUser(userId) // Flux<id>
    //		.timeout(Duration.ofMillis(500))
    //		.onErrorResume(userLocalCachedFavorites.getCachedFavorite())
    //		.switchIfEmpty(movieSuggestionService.getSuggestion()) // Flux<id>
    //      .flatMap(id -> movieDetailService.detail(id))
    //     .take(5)
    //     .subscribe(ui::ShowMovies)

//
//        fluxA.flatMap(a -> {
//            traitement(a)
//                    .onErrorReturn(e -> Tuples.fn2(e, a))
//                    .map(_ -> Tuples.fn2(null, a);
//        } ).


//        System.in.read();
    }
}
