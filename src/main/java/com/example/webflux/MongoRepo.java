package com.example.webflux;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

// CrudReactiveRepository
public interface MongoRepo extends ReactiveMongoRepository<Account, String> {
}
