package com.example.webflux;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class WebApplication {

    @Autowired
    private MongoRepo mongoRepo;

    @Bean
    public CommandLineRunner preLoadMongo() throws Exception {
        return args-> {

//            Flux.range(0,100)
//                    .map(i -> new Account(null, "name " + i, 12.3))
//                    .flatMap(mongoRepo::save)
//                    .subscribe(System.out::println);

            mongoRepo
                    .saveAll(
                        Flux.just(new Account(null, "John", 12.3),
                                  new Account(null, "Bob", 14.3),
                                   new Account(null, "Sally", 10.3)))
                    .thenMany(mongoRepo.findAll().map(a -> a.toString()))
                    .subscribe(System.out::println);
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
}
