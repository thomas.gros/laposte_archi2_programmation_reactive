package com.example.webflux;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Flux;

import java.time.Duration;

// @RestController = @Controller + @ResponseBody
@Controller
public class ReactiveController {

    @Autowired
    private MongoRepo mongoRepo;

    @GetMapping(path="/helloworld",
                produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Flux<String> sayHello() {
        return Flux.just("hello", "world");
    }

    @GetMapping(path="/accounts",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Flux<Account> getAccountList() {
        return Flux.just(new Account("1", "John", 12.3),
                         new Account("2", "Bob", 14.3),
                         new Account("3", "Sally", 10.3))
                .delayElements(Duration.ofSeconds(1));
    }

    @GetMapping(path="/accounts-sse",
            produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseBody
    public Flux<Account> getAccountListSSE() {
//        return Flux.just(new Account("1", "John", 12.3),
//                         new Account("2", "Bob", 14.3),
//                         new Account("3", "Sally", 10.3))
//                    .delayElements(Duration.ofSeconds(1));

        return mongoRepo.findAll().delayElements(Duration.ofSeconds(1));


    }
}
